document.addEventListener('DOMContentLoaded', function () {
	//Card options
	const cardArray = [
		{
			name: 'fries',
			img: 'images/fries.png'
		},
		{
			name: 'fries',
			img: 'images/fries.png'
		},
		{
			name: 'cheeseburger',
			img: 'images/cheeseburger.png'
		},
		{
			name: 'cheeseburger',
			img: 'images/cheeseburger.png'
		},
		{
			name: 'hotdog',
			img: 'images/hotdog.png'
		},{
			name: 'hotdog',
			img: 'images/hotdog.png'
		},
		{
			name: 'ice-cream',
			img: 'images/ice-cream.png'
		},
		{
			name: 'ice-cream',
			img: 'images/ice-cream.png'
		},
		{
			name: 'milkshake',
			img: 'images/milkshake.png'
		},
		{
			name: 'milkshake',
			img: 'images/milkshake.png'
		},
		{
			name: 'pizza',
			img: 'images/pizza.png'
		},
		{
			name: 'pizza',
			img: 'images/pizza.png'
		},
	]

	// console.log(cardArray);
	// console.log(cardArray.length);

	//Reseting the game
	cardArray.sort(() => 0.5 - Math.random())

	//Declaring all the variables needed 
	const grid = document.querySelector('.grid');
	const resultDisplay = document.querySelector('#result');
	var cardsChosen = [];
	var cardsChosenId = [];
	var cardsWon = [];

	//create your board

	function createBoard () {
		console.log(cardArray);
		for (let i = 0; i < cardArray.length; i++) {
			var card = document.createElement('img');
			card.setAttribute('src', 'images/blank.png')
			card.setAttribute('data-id', i)
			//Invoking the flip card function
			card.addEventListener('click', flipCard);
			//Putting all the code of the child inside the grid
			grid.appendChild(card);
		}
	}

	//Check for Matches
	function checkForMatch() {
		var cards = document.querySelectorAll('img');
		// console.log(cards);

		const optionOneId = cardsChosenId[0];
		const optionTwoId = cardsChosenId[1];
		
		console.log(optionOneId);
		console.log(optionTwoId);

		if (cardsChosen[0] === cardsChosen[1]) {
			alert('You found a match!');
			cards[optionOneId].setAttribute('src', 'images/white.png');
			cards[optionTwoId].setAttribute('src', 'images/white.png');
			cardsWon.push(cardsChosen);
		} else {
			cards[optionOneId].setAttribute('src', 'images/blank.png');
			cards[optionTwoId].setAttribute('src', 'images/blank.png');
			alert('Sorry, try again!')
		}

		//Emptying again for another set of cards to be chosen
		
		cardsChosen = [];
		cardsChosenId = [];
		
		//Displaying the score
		resultDisplay.textContent = cardsWon.length;
		
		//If the user has won and found all cards
		if (cardsWon.length === cardArray.length/2) {
			resultDisplay.textContent = 'Congratulations! You found them all!';
		}
 	}

	//Flip your Card

	function flipCard () {
		var cardId = this.getAttribute('data-id');
		console.log(cardId);
		console.log(cardsChosen);
		console.log(cardsChosenId);

		cardsChosen.push(cardArray[cardId].name);
		cardsChosenId.push(cardId);

		this.setAttribute('src', cardArray[cardId].img);
			if (cardsChosen.length === 2){
				setTimeout(checkForMatch, 500);
			}
	}

	//Invoking all the functions
	createBoard()
})

