const squares = document.querySelectorAll('.square');
const mole = document.querySelectorAll('.mole');
const timeLeft = document.querySelector('#time-left');

let score = document.querySelector('#score');

let result = 0;
let currentTime = timeLeft.textContent;

function randomSquare() {
	squares.forEach(function(className){
		className.classList.remove('mole');
	})

	let randomPosition = squares[Math.floor(Math.random() *9)];

	randomPosition.classList.add('mole');
	//console.log(randomPosition);

	//assign the id of the randomPosition to hitPosition for us to use later
	hitPosition = randomPosition.id;
}

squares.forEach(function(square){
	square.addEventListener('click', function(){
		if (square.id === hitPosition){
			result = result + 1;
			score.textContent = result;
		}
	})
})

function moveMole(){
	let timerId = null;
	timerId = setInterval(randomSquare, 1000)
}

moveMole()

function countDown(){
	currentTime --;
	console.log(currentTime);
	timeLeft.textContent = currentTime;

	if (currentTime === 0) {
		clearInterval(timerId);
		alert('GAME OVER! Your final score is' + result)
	}
}

//Setting the timer and invoking it every 1 second;
let timerId = setInterval(countDown, 1000);